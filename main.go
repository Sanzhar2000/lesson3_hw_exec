package main

import (
	"fmt"
	task5 "gitlab.com/Sanzhar2000/lesson3_hw_lib/Five_Fibonacci"
	task4 "gitlab.com/Sanzhar2000/lesson3_hw_lib/Four_Imports_Sorting_In_File"
	task1 "gitlab.com/Sanzhar2000/lesson3_hw_lib/One_Int_To_String_Conversion"
	task6 "gitlab.com/Sanzhar2000/lesson3_hw_lib/Six_Rune_By_Index"
	task3 "gitlab.com/Sanzhar2000/lesson3_hw_lib/Three_String_Reverse"
	task2 "gitlab.com/Sanzhar2000/lesson3_hw_lib/Two_String_To_Int_Conversion"
)

func main() {
	//Task 1
	fmt.Println("Task 1 result:")
	fmt.Println(task1.Itoa(-123456))

	//Task 2 without error case
	val, err := task2.Atoi("123")
	fmt.Println("Task 2 result without error case:")
	if err == nil {
		fmt.Println(val)
	}
	//Task 2 with error case
	val, err = task2.Atoi("adas123")
	fmt.Println("Task 2 result with error case:")
	if err != nil {
		fmt.Printf("error occurred: %s\n", err)
	}

	//Task 3 with latin characters
	fmt.Println("Task 3 result latin characters:")
	fmt.Println(task3.Reverse("abcde"))
	//Task 3 with Cyrillic characters
	fmt.Println("Task 3 result Cyrillic characters:")
	fmt.Println(task3.Reverse("абвгд"))

	//Task 4
	fmt.Println("Task 4 result can be found at test/test.go file")
	task4.SortImports("test/test.go")

	//Task 5
	fmt.Println("Task 5 result:")
	generator := task5.Fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Print(generator(), " ")
	}
	fmt.Print("\n")

	//Task 6 without error case
	str, index := "abcde", 3
	fmt.Println("Task 6 result without error case:")
	valRune, errRune := task6.RuneByIndex(&str, &index)
	if errRune == nil {
		fmt.Println(valRune)
	}
	//Task 6 error case
	str, index = "abcde", 6
	fmt.Println("Task 6 result with error case:")
	valRune, errRune = task6.RuneByIndex(&str, &index)
	if errRune != nil {
		fmt.Printf("Error occured: %v\n", errRune)
	}
}
